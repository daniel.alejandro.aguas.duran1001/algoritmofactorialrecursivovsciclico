/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadorafactoriaiterativo;
/**
 *
 * @author DAAD
 */
public class CalculadoraFactoriaIterativo {
    
    /**
		Factorial ITERATIVO con ciclo while en Java
	*/
    
    public static long factorial(long numero) {
		if (numero < 0)
			numero = numero * -1;
		if (numero <= 0)
			return 1;
		long factorial = 1;
		while (numero > 1) {
			factorial = factorial * numero;
			numero--;
		}
		return factorial;
	}
    /**
		Factorial calculado con recursión en Java (Solo para comparar)
	*/
	public static long factorialRecursivo(long numero) {
		if (numero <= 1)
			return 1;
		return numero * factorialRecursivo(numero - 1);
	}
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        for(long i = 0; i < 20; i++){
            System.out.println(String.format("Calculando factorial de %d. Iterativo: %d | Recursivo: %d", i, factorial(i), factorialRecursivo(i)));
		}
	}
        // TODO code application logic here
    }
    

